﻿using KillTheOverlord.Card;
using KillTheOverlord.CharacterCards;
using KillTheOverlord.Game;
using KillTheOverlord.PlotCards;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Reflection;
using System.Text;

namespace KillTheOverlord.Game
{

    public class CardDatabase
    {
        private static readonly Dictionary<string, PlotCardBase> PlotCards;
        private static readonly Dictionary<string, CharacterBase> CharactersBase;
        private static readonly Dictionary<int, PlotCardBase> NumberPlotCardList;
        private static readonly Random rand = new Random();
        private static int PlotCardCount {
            get
            {
                return NumberPlotCardList.Count;
            }
        }

        public static ReadOnlyCollection<PlotCardBase> PlotCardsList { get { return new List<PlotCardBase>(PlotCards.Values).AsReadOnly(); } }
        public static ReadOnlyCollection<CharacterBase> CharactersBaseList { get { return new List<CharacterBase>(CharactersBase.Values).AsReadOnly(); } }

        static CardDatabase()
        {
            CharactersBase = new Dictionary<string, CharacterBase>();
            PlotCards = new Dictionary<string, PlotCardBase>();

            RegisterCharacterCard(new GeneralCharacter());
            RegisterCharacterCard(new OverlordCharacter());
            RegisterCharacterCard(new PeasantCharacter());
            RegisterCharacterCard(new ConspirerCharacter());
            RegisterCharacterCard(new KnightCharacter());
            RegisterCharacterCard(new AdvisorCharacter());
            RegisterCharacterCard(new CapitanCard());
            RegisterCharacterCard(new GravediggerCharacter());
            RegisterCharacterCard(new AnarchistCharacter());
            RegisterCharacterCard(new ServantCharacter());
            RegisterCharacterCard(new SquireCharacter());

            RegisterPlotCard(new MakeExamplePlotCard());
            RegisterPlotCard(new ConspirePlotCard());
            RegisterPlotCard(new CoercionPlotCard());
            RegisterPlotCard(new RevengePlotCard());
            RegisterPlotCard(new RevolutionPlotCard());
            RegisterPlotCard(new UprisingPlotCard());
            RegisterPlotCard(new VictimizePlotCard());
            RegisterPlotCard(new SidestepPlotCard());
            RegisterPlotCard(new ExtortionPlotCard());
            RegisterPlotCard(new CapitalizePlotCard());
            RegisterPlotCard(new PleadGuiltyPlotCard());
            RegisterPlotCard(new MartyroomPlotCard());
            RegisterPlotCard(new RevoltPlotCard());

            Console.WriteLine("Register " + CharactersBaseList);

            NumberPlotCardList = new Dictionary<int, PlotCardBase>();
            int j = 0;
            foreach(PlotCardBase b in PlotCards.Values)
            {
                for (int i = 0; i < b.MaxCardCount; i++)
                {
                    NumberPlotCardList[j++] = b;
                }
            }
            Console.WriteLine(PlotCardCount);

            Random rand = new Random(DateTime.Now.Millisecond);
            for (int i = 0; i < 20; i++)
            {
                
                Console.WriteLine(rand.Next(PlotCardCount) + " random card: " + GetRandomPlotCardNew().UniqueName);
            }
        }

        /// <summary>
        /// Registers CharacterBase card with it's unique id
        /// </summary>
        /// <param name="card"></param>
        private static void RegisterCharacterCard(CharacterBase card)
        {
            CharactersBase[card.UniqueName] = card;
        }
        /// <summary>
        /// Registers Plot card with it's unique id
        /// </summary>
        /// <param name="card"></param>
        private static void RegisterPlotCard(PlotCardBase card)
        {
            PlotCards[card.UniqueName] = card;
        }
        /// <summary>
        /// Gets CharacterBase card by uuid
        /// </summary>
        /// <param name="uqid">UUID of the card</param>
        /// <returns>Card found, otherwise null</returns>
        public static CharacterBase GetCardByUniqueID(string uqid)
        {
            return CharactersBase[uqid];
        }

        /// <summary>
        /// Gets random Plot card
        /// </summary>
        /// <param name="process">If Not null - checks maximum cards count</param>
        /// <returns>Random Plot Card, if not found, null</returns>
        internal static PlotCardBase GetRandomPlotCardNew(GameProcess process = null)
        {
            
            PlotCardBase ba = NumberPlotCardList[rand.Next(NumberPlotCardList.Count)];
            if (process == null)
            {
                return ba;
            }
            else
            {
                int i = 0;
                do
                {
                    if (process.GetCardsCount(ba) >= ba.MaxCardCount)
                    {
                        ba = NumberPlotCardList[rand.Next(NumberPlotCardList.Count)];
                    }
                    else
                    {
                        return ba;
                    }

                } while (i++ < 10);
                return null;
            }
        }

        private static PlotCardBase GetPlotCardByNumber(int n)
        {
            return NumberPlotCardList[n];
        }
    }
}

