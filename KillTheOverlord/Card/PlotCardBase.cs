﻿using KillTheOverlord.Game;    
using System;
using System.Collections.Generic;
using System.Text;

namespace KillTheOverlord.Card
{
    public abstract class PlotCardBase
    {
        public readonly string UniqueName;
        public readonly PlotCardType CardType;
        public readonly int MaxCardCount;

        public PlotCardBase(string uniqueName, PlotCardType cardType, int maxCardCount)
        {
            UniqueName = uniqueName;
            CardType = cardType;
            MaxCardCount = maxCardCount;
        }

        /// <summary>
        /// Called when you are using this card
        /// </summary>
        /// <param name="executor">Who is executing</param>
        /// <param name="receiver">Who is a receiver of card, can be null</param>
        /// <param name="game">GameProcess, in which exchange took place</param>
        /// <param name="errorCode">If any error, pass correct error code</param>
        /// <returns></returns>
        public abstract bool UseCard(Player executor, Player receiver, GameProcess game, out ErrorCodes errorCode);

        /// <summary>
        /// Gets list of players on which current card can be executed
        /// </summary>
        /// <param name="cardOwner">Card owner</param>
        /// <param name="process">Process in which you want to check</param>
        /// <returns></returns>
        public abstract List<Player> GetPlayersWhoCanBeReceiver(Player cardOwner, GameProcess process);

        public override string ToString()
        {
            return UniqueName + ":" + MaxCardCount + ":" + CardType;
        }
    }
}
