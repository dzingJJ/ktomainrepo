﻿using KillTheOverlord.Game;

namespace KillTheOverlord.Card
{
    /// <summary>
    /// Base class for storing card data
    /// </summary>
    public abstract class CharacterBase
    {
        /// <summary>
        /// Unique name of card, should be code name, localization is performed in other class <see cref="TODO XD"/>
        /// </summary>
        public readonly string UniqueName;
        /// <summary>
        /// Determines how much gold must be given during budget round
        /// </summary>
        public readonly int GoldPerTurn;
        /// <summary>
        /// Determines how much health does character have (TODO?)
        /// </summary>        
        public readonly int Rank;
        /// <summary>
        /// Max Plot Card Limit, Default Value is 4
        /// </summary>
        public readonly int PlotCardLimit;
        /// <summary>
        /// If true, Passive can be executed, otherwise passive is trigerred by other event
        /// </summary>
        public readonly bool PassiveExecutable;

        public CharacterBase(string uniqueName, int goldPerTurn, int rank, bool passiveExecutable = false, int plotCardLimit = 4)
        {
            UniqueName = uniqueName;
            GoldPerTurn = goldPerTurn;
            Rank = rank;
            PlotCardLimit = plotCardLimit;
            PassiveExecutable = passiveExecutable;
        }

        /// <summary>
        /// Executes Passive
        /// </summary>
        /// <param name="executor"></param>
        /// <param name="receiver"></param>
        /// <param name="game"></param>
        /// <returns></returns>
        public abstract bool ExecutePassive(Player executor, Player receiver, GameProcess game, out ErrorCodes error);

        /// <summary>
        /// Checks Special Actions
        /// </summary>
        /// <param name="action">Special Action to check</param>
        /// <param name="arguments">Arguments</param>
        /// <param name="returnType">Return object</param>
        /// <returns>Depends on implementation</returns>
        public abstract bool SpecialAction(int action, object[] arguments, out object returnType);

    }
}
