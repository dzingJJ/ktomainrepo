﻿using System;
using System.Collections.Generic;
using System.Text;

namespace KillTheOverlord.Game
{
    
    /// <summary>
    /// List of all possible game statuses
    /// </summary>
    public enum GameStatus
    {
        /// <summary>
        /// Game Not Started Yet
        /// </summary>
        NOT_STARTED,
        /// <summary>
        /// Giving execution between players
        /// </summary>
        EXECUTOR_GIVING,
        /// <summary>
        /// Moment when people receive their money
        /// </summary>
        INCOME,
        /// <summary>
        /// Moment to give plot cards
        /// </summary>
        DRAFT,
        /// <summary>
        /// Moment to give character cards
        /// </summary>
        CHARACTER_RESHUFFLE,
        /// <summary>
        /// When character EXCEPT overlord is killed (See <see cref="OVERLORD_KILLED"/>
        /// </summary>
        CHARACTER_KILLED,
        /// <summary>
        /// Only when overlord is killed
        /// </summary>
        OVERLORD_KILLED,
        /// <summary>
        /// When game is finished
        /// </summary>
        END_OF_GAME,
    }
}
