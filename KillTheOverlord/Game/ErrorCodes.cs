﻿using System;
using System.Collections.Generic;
using System.Text;

namespace KillTheOverlord.Game
{
    public enum ErrorCodes
    {
        NO_ERROR, UNSUPPORTED_OPERATION, EXECUTION_ALREADY_EXECUTED, PLAYER_DONT_HAVE_CARD, BOOK_WHILE_HAVING_SHIELD, EXECUTOR_EQUALS_TARGET,
        PLAYER_DONT_HAVE_EXECUTION, GIVE_FIRST_OVERLORD, GAME_FINISHED
    }
}
