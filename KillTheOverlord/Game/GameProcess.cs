﻿using KillTheOverlord.Card;
using KillTheOverlord.Game;
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using System.Timers;
using KillTheOverlord.Events;

namespace KillTheOverlord.Game
{
    public enum DataKeys
    {
        EXECUTION_GIVEN, LAST_EXECUTION_OWNER, DEATH_ORDER
    }
    /// <summary>
    /// ONE MINUTE LIMIT TIME
    /// </summary>
    public class GameProcess
    {
        public GameData Game { get; private set; }
        private RankComparer RankComparer = new RankComparer();
        private PlayerComparer PlayerComparer = new PlayerComparer();

        private Dictionary<DataKeys, object> Data = new Dictionary<DataKeys, object>();
        /// <summary>
        /// Creates New GameProcess
        /// TODO: Checking number of characters
        /// </summary>
        /// <param name="players">PlayerDetails to create players</param>
        public GameProcess(PlayerDetails[] players)
        {
            Game = new GameData(players);
        }
        /// <summary>
        /// Starts Thread TODO
        /// </summary>
        public void StartThread()
        {

        }

        /// <summary>
        /// Tick of the game, some events uses it, you need to execute is twice per second (500ms)
        /// If you don't do any implementation, you can use method
        /// </summary>
        public void Tick()
        {

        }
        /// <summary>
        /// Gets data from Data dictionary
        /// </summary>
        /// <typeparam name="T">Autocast type</typeparam>
        /// <param name="code"><see cref="DataKeys"/> to identify code</param>
        /// <param name="defaultValue">Default value (it will be put in the array)</param>
        /// <returns>Object T type if found in dictionary, otherwise default</returns>
        public T GetData<T>(DataKeys code, object defaultValue = null)
        {
            if (Data.ContainsKey(code))
            {
                return (T)Data[code];
            }
            else
            {
                Data[code] = defaultValue;
                return (T)defaultValue;
            }
        }
        /// <summary>
        /// Sets data to system
        /// </summary>
        /// <param name="code">DataKey to set</param>
        /// <param name="value">Value to set</param>
        private void SetData(DataKeys code, object value)
        {
            Data[code] = value;
        }
        /// <summary>
        /// Starts Game and reshuffle characters
        /// </summary>
        public void StartGame()
        {
            /*
             *  The begining of the game
             *  Shuffle character cards
             *  Give everybody 5 gold
             *  Give everybody 4 cards
             */
            EventsManager.CallEvent(new PreGameStartEvent(this));
            SetData(DataKeys.DEATH_ORDER, new List<Player>());
            GiveOutCharactersRandom(); //TODO: Debug option mayby?
            ModifyCashOfAllPlayers(5);
            AddPlotCardsToAllPlayers(4, true);
            Game.PlayerWithExecution = Game.GetOverlord();
        }
        /// <summary>
        /// Gets characters used in this game
        /// </summary>
        /// <returns>List of characters used in this game</returns>
        public CharacterBase[] GetCharactersPool()
        {
            CharacterBase[] arra = new CharacterBase[Game.Players.Count];
            for (int i = 0; i < arra.Length; i++)
            {
                arra[i] = Game.Players[i].CurrentCharacter;
            }
            return arra;
        }
        /// <summary>
        /// Adds cards to every player
        /// TODO: Some characters can have more then 4 cards. Workaround needed
        /// </summary>
        /// <param name="fourCards">if true, add 4 cards to all players, else adds 2 cards to max 4 cards</param>
        private void AddPlotCardsToAllPlayers(int cards, bool force = false)
        {
            foreach (Player player in Game.Players)
            {
                if (player.Died)
                {
                    continue;
                }
                player.AddPlotCardsAsManyAsCan(cards, force, this);
            }
        }

        /// <summary>
        /// Modifies cash of all players
        /// </summary>
        /// <param name="gold">How to modify cash</param>
        private void ModifyCashOfAllPlayers(int gold) //income, called after every execution
        {
            foreach (Player p in Game.Players)
            {
                EventsManager.CallEvent(new CashAddEvent(p, gold, this));
                p.ModifyMoney(gold);
            }
        }
        /// <summary>
        /// Trying to use given card
        /// </summary>
        /// <param name="executor">Players who is using this card</param>
        /// <param name="receiver">Players who is receiver of this card, can be null</param>
        /// <param name="usedCard">Card used by executor</param>
        /// <param name="error">If returns false, error caused system not executing card</param>
        /// <returns>True if sucess, false and ErrorCode when failed</returns>
        public bool UsePlotCard(Player executor, Player receiver, PlotCardBase usedCard, out ErrorCodes error)
        {
            error = ErrorCodes.NO_ERROR;
            if (Game.GameFinished)
            {
                error = ErrorCodes.GAME_FINISHED;
                return false;
            }
            if (!executor.HasCard(usedCard))
            {
                error = ErrorCodes.PLAYER_DONT_HAVE_CARD;
                return false;
            }
            if (usedCard.CardType == PlotCardType.BOOK && executor.HasCardOfGivenType(PlotCardType.SHIELD))
            {
                error = ErrorCodes.BOOK_WHILE_HAVING_SHIELD;
                return false;
            }
            bool b = usedCard.UseCard(executor, receiver, this, out error);
            if (b)
            {
                EventsManager.CallEvent(new PlotCardUseEvent(executor, receiver, this, usedCard));
                executor.RemovePlotCard(usedCard);
            }
            return b;
        }
        /// <summary>
        /// Executes passive from executer on receiver
        /// </summary>
        /// <param name="executor"></param>
        /// <param name="receiver"></param>
        /// <param name="error"></param>
        /// <returns></returns>
        public bool UsePassive(Player executor, Player receiver, out ErrorCodes error)
        {
            if (Game.GameFinished)
            {
                error = ErrorCodes.GAME_FINISHED;
                return false;
            }
            bool b = executor.CurrentCharacter.ExecutePassive(executor, receiver, this, out error);
            if (b)
            {
                EventsManager.CallEvent(new PassiveUseEvent(executor, receiver, this));
                executor.PassiveUsed = true;
            }
            return b;
        }

        /// <summary>
        /// Moves the execution order to given player and call the event
        /// </summary>
        /// <param name="from">Player to bring execution from</param>
        /// <param name="to">Player to give execution for</param>
        /// <returns></returns>
        internal bool MoveExecution(Player from, Player to, out ErrorCodes error)
        {
            if (Game.GameFinished)
            {
                error = ErrorCodes.GAME_FINISHED;
                return false;
            }
            error = ErrorCodes.NO_ERROR;
            if (from.Equals(to))
            {
                error = ErrorCodes.EXECUTOR_EQUALS_TARGET;
                return false;
            }
            EventsManager.CallEvent(new ExecutionGiveEvent(from, to, this));
            Game.PlayerWithExecution = to;
            Data[DataKeys.LAST_EXECUTION_OWNER] = from;
            return true;
        }

        /// <summary>
        /// Claim that players wants to die
        /// </summary>
        /// <param name="executor">Executor of claim</param>
        /// <param name="error">If returns false, error returned</param>
        /// <returns>True if players successfully died, else false</returns>
        public bool ClaimDeath(Player executor, out ErrorCodes error)
        {
            error = ErrorCodes.NO_ERROR;
            if (Game.GameFinished)
            {
                error = ErrorCodes.GAME_FINISHED;
                return false;
            }
            if (!Game.PlayerWithExecution.Equals(executor))
            {
                error = ErrorCodes.PLAYER_DONT_HAVE_EXECUTION;
                return false;
            }
            if (GetData<bool>(DataKeys.EXECUTION_GIVEN, false))
            {
                error = ErrorCodes.GIVE_FIRST_OVERLORD;
                return false;
            }
            Player killer = GetData<Player>(DataKeys.LAST_EXECUTION_OWNER);
            if (executor.CurrentCharacter.Equals(CardDatabase.GetCardByUniqueID("overlord")))
            {
                DeathOverlord(executor, killer);
                return true;
            }
            else
            {
                DeathNonOverlord(executor, killer);
                return true;
            }
        }

        private void DeathNonOverlord(Player killed, Player killer)
        {
            /*
             * After  death of nonoverlord:
             * - give cash
             * - check win
             * - give 2 cards
             */
            killed.Died = true;
            StartRound();
            GetData<List<Player>>(DataKeys.DEATH_ORDER).Add(killed);
            EventsManager.CallEvent(new DeathEvent(killer, killed, this));
        }
        /// <summary>
        /// Called when someone dies and game starts new round
        /// </summary>
        private void StartRound()
        {
            GiveCashBasedOnCharacters();
            if (CheckWinCondition(out Player winner))
            {
                EventsManager.CallEvent(new WinEvent(winner, this));
                Game.GameFinished = true;
            }
            AddPlotCardsToAllPlayers(2);
        }
        /// <summary>
        /// Called when overlord died
        /// </summary>
        /// <param name="killed">Person killed</param>
        /// <param name="killer">Killer</param>
        private void DeathOverlord(Player killed, Player killer)
        {
            EventsManager.CallEvent(new DeathEvent(killer, killed, this));
            RearangeCharacters();
            ResetDeath();
            StartRound();
        }

        private void ResetDeath()
        {
            foreach (Player pl in Game.Players)
            {
                pl.Died = false;
            }
            GetData<List<Player>>(DataKeys.DEATH_ORDER).Clear();
        }

        /// <summary>
        /// Giving new characters to players
        /// </summary>
        internal void RearangeCharacters()
        {
            List<Player> died = GetData<List<Player>>(DataKeys.DEATH_ORDER);
            List<Player> living = new List<Player>();
            foreach (Player p in Game.Players)
            {
                if (!p.Died)
                {
                    living.Add(p);
                }
            }
            CharacterBase[] Pool = GetCharactersPool();
            Array.Sort(Pool, RankComparer);
            living.Sort(PlayerComparer);

            int i = 0;
            foreach (Player player in living)
            {
                player.SetCharacterCard(Pool[i++]);
            }

            foreach(Player player in died)
            {
                player.SetCharacterCard(Pool[i++]);
            }
            /*
             * And how now? 
             * LIVING
             * Biggest to Lowest
             * DEATH:
             * First To Last
             */
        }

        /// <summary>
        /// Checks if overlord have cash or overlord is only player lived
        /// <paramref name="winner">Player, who is a winner, otherwise null</paramref>
        /// <returns>True if someone wins, otherwise false</returns>
        /// </summary>
        private bool CheckWinCondition(out Player winner)
        {
            if (Game.GetOverlord().Coins >= 30 || Game.OverlordOnlyLiving())
            {
                winner = Game.GetOverlord();
                return true;
            }
            else
            {
                winner = null;
                return false;
            }
        }

        /// <summary>
        /// Give cash to living people based on their characters
        /// </summary>
        private void GiveCashBasedOnCharacters()
        {
            foreach (Player player in Game.Players)
            {
                if (!player.Died)
                {
                    player.AddCoinsByCharacters(this);
                }
            }
        }

        /// <summary>
        /// Gives random characters to players
        /// </summary>
        /// <param name="characters">If you want to limit character pool, pass list here</param>
        internal void GiveOutCharactersRandom(CharacterBase[] characters = null)
        {
            List<CharacterBase> chars = new List<CharacterBase>();
            if (characters != null)
            {
                chars.AddRange(characters);
            }
            else
            {
                chars.AddRange(CardDatabase.CharactersBaseList);
            }
            CharacterBase[] cara = new CharacterBase[Game.Players.Count];
            Random ran = new Random();
            for (int i = 0; i < cara.Length; i++)
            {
                int n = ran.Next(chars.Count);
                CharacterBase b = chars[n];
                cara[i] = b;
                chars.Remove(b);
            }
            if (!cara.Contains(CardDatabase.GetCardByUniqueID("overlord")))
            {
                cara[ran.Next(cara.Length)] = CardDatabase.GetCardByUniqueID("overlord");
            }
            int j = 0;
            foreach (var player in Game.Players)
            {
                player.SetCharacterCard(cara[j++]);
            }
            EventsManager.CallEvent(new CharacterReshuffleEvent(true, this));

        }
        /// <summary>
        /// Gets how much of given card is on the deck
        /// </summary>
        /// <param name="cardBase">Card to check</param>
        /// <returns>Numbers of cards</returns>
        public int GetCardsCount(PlotCardBase cardBase)
        {
            int a = 0;
            foreach (Player player in Game.Players)
            {
                a += (player.PlotCards.Count(delegate (PlotCardBase plot)
                 {
                     return plot.Equals(cardBase);
                 }));
            }
            return a;
        }
        /// <summary>
        /// Outputs debug output on console
        /// </summary>
        public void DebugPrint()
        {
            w("DEBUG OUTPUT OF GAME");
            w("Game created: " + Game != null);
            w("---------------------");
            Game.DebugPrint();
            w("---------------------");

        }

        internal static void w(object o)
        {
            Console.WriteLine(o);
        }

    }

    /// <summary>
    /// List of all possible actions connected with player
    /// </summary>
    public enum Actions
    {
        /// <summary>
        /// Action of using plot card
        /// Arguments needed: 
        /// args[0] = used plot card
        /// Some cards may need other arguments
        /// </summary>
        PLOT_CARD_USE,
        /// <summary>
        /// Action of giving someone execution, NOT USING PLOT CARD
        /// No arguments needed
        /// </summary>
        //EXECUTION_GIVE,
        /// <summary>
        /// Death of player
        /// </summary>
        DEATH,
        /// <summary>
        /// Action of giving money from bank, not from using plot card
        /// </summary>
        INCOME,
        /// <summary>
        /// Reshuffle of characters, not from 
        /// </summary>
        DRAFT,
        /// <summary>
        /// Usage of character passive
        /// </summary>
        PASSIVE
    }

    internal class RankComparer : IComparer<CharacterBase>
    {
        public int Compare(CharacterBase x, CharacterBase y)
        {
            return y.Rank.CompareTo(x.Rank);
        }
    }

    internal class PlayerComparer : IComparer<Player>
    {
        public int Compare(Player x, Player y)
        {
            return y.CurrentCharacter.Rank.CompareTo(x.CurrentCharacter.Rank);
        }
    }
}
