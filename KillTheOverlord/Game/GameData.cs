﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;
using static KillTheOverlord.Game.GameProcess;
namespace KillTheOverlord.Game
{
    /// <summary>
    /// Class contains current game data
    /// </summary>
    public class GameData
    {
        /// <summary>
        /// Unmodiffable list of players in-game
        /// </summary>
        public ReadOnlyCollection<Player> Players { get { return playersPrivate.AsReadOnly(); } }

        /// <summary>
        /// Current game status
        /// </summary>
        public GameStatus GameStatus { get { return gameStatusPrivate; } }

        private GameStatus gameStatusPrivate;
        private List<Player> playersPrivate = new List<Player>();

        public Player PlayerWithExecution { get; internal set; }

        public bool GameFinished { get; internal set; }
        public GameData(PlayerDetails[] players)
        {
            GameFinished = false;
            foreach (var play in players)
            {
                playersPrivate.Add(new Player(play));
            }
        }

        public void DebugPrint()
        {
            w("GameData DATA");
            w("Players in game: " + Players.Count);
            w("GameStatus: " + GameStatus);
            w("Execution is with player: " + PlayerWithExecution);
            w("------PLAYERS-------");
            foreach (Player p in Players)
            {
                p.Debug();
                w("---");
            }
            w("------END PLAYERS------");

        }

        public Player GetPlayerByNumber(long uid)
        {
            foreach (Player player in Players)
            {
                if (player.PlayerDetails.UniqueNumber == uid)
                {
                    return player;
                }
            }
            return null;
        }

        /// <summary>
        /// Gets player who is overlord
        /// </summary>
        /// <returns>Player with overlord card, if not present return null (should never happen)</returns>
        public Player GetOverlord()
        {
            return playersPrivate.Find(delegate (Player player) { return player.CurrentCharacter.UniqueName.Equals("overlord"); });
        }

        /// <summary>
        /// Checks if overlord is the only living player
        /// NOTE: If all players are dead, method will return true anyway
        /// </summary>
        /// <returns>True if all players are death, otherwise false</returns>
        public bool OverlordOnlyLiving()
        {
            foreach(Player player in Players)
            {
                if(!player.Died && !player.CurrentCharacter.UniqueName.Equals("overlord"))
                {
                    return false;
                }
            }
            return true;
        }
    }
}
