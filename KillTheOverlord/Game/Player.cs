﻿using KillTheOverlord.Card;
using KillTheOverlord.Events;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;
using static KillTheOverlord.Game.GameProcess;

namespace KillTheOverlord.Game
{
    /// <summary>
    /// Player data, used for storage
    /// </summary>
    /// TODO: REWRITE ALL VALUES 
    public class Player
    {
        /// <summary>
        /// Current card in the hand
        /// </summary>
        public CharacterBase CurrentCharacter { get { return currCard; } }
        public ReadOnlyCollection<PlotCardBase> PlotCards { get { return listOfPlotCards.AsReadOnly(); } }
        public readonly PlayerDetails PlayerDetails;
        public int Coins { get; internal set; }
        public int PlotCardCount { get { return listOfPlotCards.Count; } }
        public bool Died { get; internal set; }

        public bool PassiveUsed { get; internal set; }
        private List<PlotCardBase> listOfPlotCards;
        private CharacterBase currCard;

        public Player(PlayerDetails play)
        {
            this.PlayerDetails = play;
            listOfPlotCards = new List<PlotCardBase>();
        }

        /// <summary>
        /// Sets current character card
        /// </summary>
        /// <param name="chara">Character Card</param>
        internal void SetCharacterCard(CharacterBase chara)
        {
            this.currCard = chara;
        }
        /// <summary>
        /// Modify coins count
        /// </summary>
        /// <param name="coins">How to modify coins</param>
        internal void ModifyMoney(int coins)
        {
            this.Coins += coins;
        }
        /// <summary>
        /// Adds card to players
        /// </summary>
        /// <param name="b"></param>
        internal void AddPlotCard(PlotCardBase b, GameProcess process)
        {
            EventsManager.CallEvent(new PlotCardAddEvent(b, this, process));
            listOfPlotCards.Add(b);
        }
        /// <summary>
        /// Removes plot card from player
        /// </summary>
        /// <param name="b"></param>
        internal void RemovePlotCard(PlotCardBase b)
        {
            listOfPlotCards.Remove(b);
        }

        public void Debug()
        {
            w("PLAYER ");
            w("Player Data: " + PlayerDetails.ToString());
            w("Current Card: " + CurrentCharacter.UniqueName);
            w("Coins: " + Coins);
            w("PlotCards:\n-----------\n" + string.Join("\n**************\n", listOfPlotCards.ToArray().ToStringArray()));
        }

        /// <summary>
        /// Checks if player has card on deck
        /// </summary>
        /// <param name="card">Card to check </param>
        /// <returns>True if have, otherwise false</returns>
        public bool HasCard(PlotCardBase card)
        {
            return listOfPlotCards.Contains(card);
        }
        /// <summary>
        /// Checks if player has any card of given type
        /// </summary>
        /// <param name="type">Type to check</param>
        /// <returns>True if have, false if not</returns>
        public bool HasCardOfGivenType(PlotCardType type)
        {
            return listOfPlotCards.Exists(delegate (PlotCardBase card)
            {
                return card.CardType == type;
            });
        }
        /// <summary>
        /// Adds as many cards as can
        /// </summary>
        /// <param name="cards">How many cards try to add</param>
        /// <param name="force">If dont look on limit</param>
        internal void AddPlotCardsAsManyAsCan(int cards, bool force = false, GameProcess process = null)
        {
            int limit = GetMaxCardsCount();
            if(limit == 0)
            {
                limit = int.MaxValue;
            }

            for(int i = 0; i < cards && (force || PlotCardCount < limit); i++)
            {
                AddPlotCard(CardDatabase.GetRandomPlotCardNew(process), process);
            }
        }

        /// <summary>
        /// Return  max card count
        /// </summary>
        /// <returns>0 if unlimited, otherwise</returns>
        public int GetMaxCardsCount()
        {
            return CurrentCharacter.PlotCardLimit;
        }

        /// <summary>
        /// Adds coins based on character
        /// </summary>
        public void AddCoinsByCharacters(GameProcess process)
        {
            EventsManager.CallEvent(new CashAddEvent(this, CurrentCharacter.GoldPerTurn, process));
            Coins += CurrentCharacter.GoldPerTurn;
        }
    }
}
