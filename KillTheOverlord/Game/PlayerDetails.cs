﻿using System;
using System.Collections.Generic;
using System.Text;

namespace KillTheOverlord.Game
{
    public class PlayerDetails
    {
        public readonly string Nickname;
        public readonly int UniqueNumber;

        public PlayerDetails(string nickname, int uniqueNumber)
        {
            Nickname = nickname;
            UniqueNumber = uniqueNumber;
        }

        public override string ToString()
        {
            return Nickname + ":" + UniqueNumber;
        }
    }
}
