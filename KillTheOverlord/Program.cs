﻿using KillTheOverlord.Events;
using KillTheOverlord.Game;
using System;
using System.Reflection;

namespace KillTheOverlord
{
    class Program
    {
        static void action(PreGameStartEvent evt)
        {
            Console.WriteLine("EVT");
        }

        static bool IsInside(int x, int y, int r)
        {
            return x * x + y * y <= r * r;
        }

        static void Main(string[] args)
        {
            GameProcess process = new GameProcess(new PlayerDetails[] { new PlayerDetails("dz", 123124), new PlayerDetails("lol", 1234), new PlayerDetails("D", 12345)});
            process.StartGame();
            process.RearangeCharacters();
            process.DebugPrint();
            Console.Read();
        }
    }
}