﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace KillTheOverlord.Game
{
    public static class Extensions
    {
        public static string[] ToStringArray(this object[] array)
        {
            string[] s = new string[array.Length];
            for (int i = 0; i < array.Length; i++)
            {
                s[i] = array[i].ToString();
            }
            return s;
        }
    }
}
