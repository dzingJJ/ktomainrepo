﻿using KillTheOverlord.Card;
using KillTheOverlord.Game;
using System;
using System.Collections.Generic;
using System.Text;

/// <summary>
/// Need to check new cards:
/// Victimize: Give the execution order to the player 1 or 2 left, then that player discard 1 card or gives you 1 gold
/// Sidestep: Give the executon to the player 1 or 2 right, then draw a card
/// </summary>
namespace KillTheOverlord.PlotCards
{
    /// <summary>
    /// Gives card only to player with lower rank, if you are lowest, you can give it to anyone
    /// Bring one gold from banks
    /// </summary>
    public class MakeExamplePlotCard : PlotCardBase
    {
        public MakeExamplePlotCard() : base("makeexample", PlotCardType.SHIELD, 5)
        {
        }

        public override List<Player> GetPlayersWhoCanBeReceiver(Player cardOwner, GameProcess process)
        {
            throw new NotImplementedException();
        }

        public override bool UseCard(Player executor, Player receiver, GameProcess game, out ErrorCodes errorCode)
        {
            throw new NotImplementedException();
        }
    }
    /// <summary>
    /// Gives 1 person left right execution, rest people exchange their plot card
    /// </summary>
    public class ConspirePlotCard : PlotCardBase
    {
        public ConspirePlotCard() : base("conspire", PlotCardType.SHIELD, 3)
        {
        }

        public override List<Player> GetPlayersWhoCanBeReceiver(Player cardOwner, GameProcess process)
        {
            throw new NotImplementedException();
        }

        public override bool UseCard(Player executor, Player receiver, GameProcess game, out ErrorCodes errorCode)
        {
            throw new NotImplementedException();
        }
    }

    /// <summary>
    /// Gives 1 player left or right Execution, he gives you plotcard or you bring 2 gold from bank
    /// </summary>
    public class CoercionPlotCard : PlotCardBase
    {

        public CoercionPlotCard() : base("coercion", PlotCardType.SHIELD, 3)
        {
        }

        public override List<Player> GetPlayersWhoCanBeReceiver(Player cardOwner, GameProcess process)
        {
            throw new NotImplementedException();
        }

        public override bool UseCard(Player executor, Player receiver, GameProcess game, out ErrorCodes errorCode)
        {
            throw new NotImplementedException();
        }
    }

    /// <summary>
    /// Kill yourself and after that player who gave you execution card
    /// </summary>
    public class RevengePlotCard : PlotCardBase
    {
        public RevengePlotCard() : base("revenge", PlotCardType.BOOK, 1)
        {
        }

        public override List<Player> GetPlayersWhoCanBeReceiver(Player cardOwner, GameProcess process)
        {
            throw new NotImplementedException();
        }

        public override bool UseCard(Player executor, Player receiver, GameProcess game, out ErrorCodes errorCode)
        {
            throw new NotImplementedException();
        }
    }
    /// <summary>
    /// Randomly gives characters and ends turn
    /// </summary>
    public class RevolutionPlotCard : PlotCardBase
    {
        public RevolutionPlotCard() : base("revolution", PlotCardType.BOOK, 1)
        {
        }

        public override List<Player> GetPlayersWhoCanBeReceiver(Player cardOwner, GameProcess process)
        {
            throw new NotImplementedException();
        }

        public override bool UseCard(Player executor, Player receiver, GameProcess game, out ErrorCodes errorCode)
        {
            throw new NotImplementedException();
        }
    }
    /// <summary>
    /// Gives someone execution, if someone discards BOOK, he dies, otherwise executor dies
    /// </summary>
    public class UprisingPlotCard : PlotCardBase
    {
        public UprisingPlotCard() : base("uprising", PlotCardType.BOOK, 3)
        {
        }

        public override List<Player> GetPlayersWhoCanBeReceiver(Player cardOwner, GameProcess process)
        {
            throw new NotImplementedException();
        }

        public override bool UseCard(Player executor, Player receiver, GameProcess game, out ErrorCodes errorCode)
        {
            throw new NotImplementedException();
        }
    }
    /// <summary>
    /// Give execution 1 or 2 person on left, execution receiver discard one card (if can't, player bring one gold)
    /// </summary>
    public class VictimizePlotCard : PlotCardBase
    {
        public VictimizePlotCard() : base("victimize", PlotCardType.SHIELD, 4)
        {
        }

        public override List<Player> GetPlayersWhoCanBeReceiver(Player cardOwner, GameProcess process)
        {
            throw new NotImplementedException();
        }

        public override bool UseCard(Player executor, Player receiver, GameProcess game, out ErrorCodes errorCode)
        {
            throw new NotImplementedException();
        }
    }
    /// <summary>
    /// Give execution 1 or 2 to player on right, and draw one card
    /// </summary>
    public class SidestepPlotCard : PlotCardBase
    {
        public SidestepPlotCard() : base("sidestep", PlotCardType.SHIELD, 4)
        {
        }

        public override List<Player> GetPlayersWhoCanBeReceiver(Player cardOwner, GameProcess process)
        {
            throw new NotImplementedException();
        }

        public override bool UseCard(Player executor, Player receiver, GameProcess game, out ErrorCodes errorCode)
        {
            throw new NotImplementedException();
        }
    }

    /// <summary>
    /// Give Execution to any player, bring 1 gold from him (if can)
    /// </summary>
    public class ExtortionPlotCard : PlotCardBase
    {
        public ExtortionPlotCard() : base("eaxtortion", PlotCardType.SHIELD, 4)
        {
        }

        public override List<Player> GetPlayersWhoCanBeReceiver(Player cardOwner, GameProcess process)
        {
            throw new NotImplementedException();
        }

        public override bool UseCard(Player executor, Player receiver, GameProcess game, out ErrorCodes errorCode)
        {
            throw new NotImplementedException();
        }
    }

    /// <summary>
    /// Give execution player 2 left right, bring 2 gold from bank
    /// </summary>
    public class CapitalizePlotCard : PlotCardBase
    {
        public CapitalizePlotCard() : base("capitalize", PlotCardType.SHIELD, 4)
        {
        }

        public override List<Player> GetPlayersWhoCanBeReceiver(Player cardOwner, GameProcess process)
        {
            throw new NotImplementedException();
        }

        public override bool UseCard(Player executor, Player receiver, GameProcess game, out ErrorCodes errorCode)
        {
            throw new NotImplementedException();
        }
    }
    /// <summary>
    /// Pledge you are guilty, and brings plot card and 1 gold
    /// </summary>
    public class PleadGuiltyPlotCard : PlotCardBase
    {
        public PleadGuiltyPlotCard() : base("pleadguilty", PlotCardType.SPECIAL, 3)
        {
        }

        public override List<Player> GetPlayersWhoCanBeReceiver(Player cardOwner, GameProcess process)
        {
            throw new NotImplementedException();
        }

        public override bool UseCard(Player executor, Player receiver, GameProcess game, out ErrorCodes errorCode)
        {
            throw new NotImplementedException();
        }
    }

    /// <summary>
    /// Kills yourself and gets 7 gold from bank
    /// </summary>
    public class MartyroomPlotCard : PlotCardBase
    {
        public MartyroomPlotCard() : base("martyroom", PlotCardType.BOOK, 1)
        {
        }

        public override List<Player> GetPlayersWhoCanBeReceiver(Player cardOwner, GameProcess process)
        {
            throw new NotImplementedException();
        }

        public override bool UseCard(Player executor, Player receiver, GameProcess game, out ErrorCodes errorCode)
        {
            throw new NotImplementedException();
        }
    }
    /// <summary>
    /// Give Execution to player one rank bigger or less, and exchange characters with him
    /// </summary>
    public class RevoltPlotCard : PlotCardBase
    {
        public RevoltPlotCard() : base("revolt", PlotCardType.SHIELD, 2)
        {
        }

        public override List<Player> GetPlayersWhoCanBeReceiver(Player cardOwner, GameProcess process)
        {
            throw new NotImplementedException();
        }

        public override bool UseCard(Player executor, Player receiver, GameProcess game, out ErrorCodes errorCode)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Cancels last action
        /// </summary>
        public class RetaliatePlotCard : PlotCardBase
        {
            public RetaliatePlotCard() : base("retaliate", PlotCardType.SPECIAL, 4)
            {
            }

            public override List<Player> GetPlayersWhoCanBeReceiver(Player cardOwner, GameProcess process)
            {
                throw new NotImplementedException();
            }

            public override bool UseCard(Player executor, Player receiver, GameProcess game, out ErrorCodes errorCode)
            {
                throw new NotImplementedException();
            }
        }
    }
}
