﻿using KillTheOverlord.Card;
using KillTheOverlord.Game;
using System;
using System.Collections.Generic;
using System.Text;

/// <summary>
/// Noew cards:
/// Biurocrat: <>
/// foreseer wrozbita
/// rapscallion - łotr
/// Merchant kupiec
/// outlaw
/// 
/// </summary>
namespace KillTheOverlord.CharacterCards
{
    /// <summary>
    /// Special actions for SpecialActo
    /// </summary>
    public class ActionsNumbers
    {
        public readonly static int PLAYERS_MODIFIER;
    }

    /// <summary>
    /// Can give card to everyone, no distance modifier
    /// </summary>
    public class GeneralCharacter : CharacterBase
    {
        public GeneralCharacter() : base("general", 4, 6, false)
        {
        }

        public override bool ExecutePassive(Player executor, Player receiver, GameProcess game, out ErrorCodes error)
        {
            throw new NotImplementedException();
        }

        public override bool SpecialAction(int action, object[] arguments, out object returnType)
        {
            throw new NotImplementedException();
        }
    }
    /// <summary>
    /// Gives execution to player
    /// </summary>
    public class OverlordCharacter : CharacterBase
    {

        public OverlordCharacter() : base("overlord", 6, 8, true)
        {
        }

        public override bool ExecutePassive(Player executor, Player receiver, GameProcess game, out ErrorCodes error)
        {
            throw new NotImplementedException();
        }

        public override bool SpecialAction(int action, object[] arguments, out object returnType)
        {
            throw new NotImplementedException();
        }
    }
    /// <summary>
    /// Can give coin and execution to player without using plot cards
    /// </summary>
    public class PeasantCharacter : CharacterBase
    {

        public PeasantCharacter() : base("peasant", 0, 2, true)
        {
        }

        public override bool ExecutePassive(Player executor, Player receiver, GameProcess game, out ErrorCodes error)
        {
            throw new NotImplementedException();
        }

        public override bool SpecialAction(int action, object[] arguments, out object returnType)
        {
            throw new NotImplementedException();
        }
    }
    /// <summary>
    /// Before round, everyone with BOOK card must be shown
    /// </summary>
    public class ConspirerCharacter : CharacterBase
    {

        public ConspirerCharacter() : base("conspirer", 4, 6, false)
        {
        }

        public override bool ExecutePassive(Player executor, Player receiver, GameProcess game, out ErrorCodes error)
        {
            throw new NotImplementedException();
        }

        public override bool SpecialAction(int action, object[] arguments, out object returnType)
        {
            throw new NotImplementedException();
        }
    }

    /// <summary>
    /// Nobody can bring gold or cards from you and force you to discard
    /// </summary>
    public class KnightCharacter : CharacterBase
    {
        public KnightCharacter() : base("knight", 2, 4, false)
        {
        }

        public override bool ExecutePassive(Player executor, Player receiver, GameProcess game, out ErrorCodes error)
        {
            throw new NotImplementedException();
        }

        public override bool SpecialAction(int action, object[] arguments, out object returnType)
        {
            throw new NotImplementedException();
        }
    }

    /// <summary>
    /// When book is played, player can ignore it's effect
    /// </summary>
    public class AdvisorCharacter : CharacterBase
    {
        public AdvisorCharacter() : base("Advisor", 5, 7, true)
        {
        }

        public override bool ExecutePassive(Player executor, Player receiver, GameProcess game, out ErrorCodes error)
        {
            throw new NotImplementedException();
        }

        public override bool SpecialAction(int action, object[] arguments, out object returnType)
        {
            throw new NotImplementedException();
        }
    }
    /// <summary>
    /// When you give execution to other player, he have to discard one card
    /// </summary>
    public class CapitanCard : CharacterBase
    {
        public CapitanCard() : base("capitan", 3, 5)
        {
        }

        public override bool ExecutePassive(Player executor, Player receiver, GameProcess game, out ErrorCodes error)
        {
            throw new NotImplementedException();
        }

        public override bool SpecialAction(int action, object[] arguments, out object returnType)
        {
            throw new NotImplementedException();
        }
    }

    /// <summary>
    /// When someone dies, bring 1 gold from him and 1 plot card from deck
    /// </summary>
    public class GravediggerCharacter : CharacterBase
    {
        public GravediggerCharacter() : base("gravedigger", 1, 3)
        {
        }

        public override bool ExecutePassive(Player executor, Player receiver, GameProcess game, out ErrorCodes error)
        {
            throw new NotImplementedException();
        }

        public override bool SpecialAction(int action, object[] arguments, out object returnType)
        {
            throw new NotImplementedException();
        }
    }

    /// <summary>
    /// Give Overlord one coin. When you don't have SHIELD card, you can play any BOOK as revolution
    /// </summary>
    public class AnarchistCharacter : CharacterBase
    {
        public AnarchistCharacter() : base("anarchist", 0, 1, true)
        {
        }

        public override bool ExecutePassive(Player executor, Player receiver, GameProcess game, out ErrorCodes error)
        {
            throw new NotImplementedException();
        }

        public override bool SpecialAction(int action, object[] arguments, out object returnType)
        {
            throw new NotImplementedException();
        }
    }

    /// <summary>
    /// Give 1 coin to Overlord every round, when someone gives you execution card and have more cards then you, draw one card
    /// </summary>
    public class ServantCharacter : CharacterBase
    {
        public ServantCharacter() : base("servant", 0, 1)
        {
        }

        public override bool ExecutePassive(Player executor, Player receiver, GameProcess game, out ErrorCodes error)
        {
            throw new NotImplementedException();
        }

        public override bool SpecialAction(int action, object[] arguments, out object returnType)
        {
            throw new NotImplementedException();
        }
    }
    /// <summary>
    /// Can draw 1 additional card per turn, starting from begining
    /// </summary>
    public class SquireCharacter : CharacterBase
    {
        public SquireCharacter() : base("squire", 1, 3, true, 6)
        {
        }

        public override bool ExecutePassive(Player executor, Player receiver, GameProcess game, out ErrorCodes error)
        {
            throw new NotImplementedException();
        }

        public override bool SpecialAction(int action, object[] arguments, out object returnType)
        {
            throw new NotImplementedException();
        }
    }
}
