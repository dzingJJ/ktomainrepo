﻿using KillTheOverlord.Card;
using KillTheOverlord.Game;
using System;
using System.Collections.Generic;
using System.Text;

namespace KillTheOverlord.Events
{
    /// <summary>
    /// Base class for creating new Events
    /// </summary>
    public abstract class Event
    {
        public bool Cancelled;

        public readonly bool CanBeCancelled;

        public Event(bool canBeCancelled)
        {
            CanBeCancelled = canBeCancelled;
        }
    }
    /// <summary>
    /// Called when someone moving execution to other player
    /// </summary>
    public class ExecutionGiveEvent : Event
    {
        public readonly Player From, To;
        public readonly GameProcess Process;

        public ExecutionGiveEvent(Player from, Player to, GameProcess process) : base(false)
        {
            From = from;
            To = to;
            Process = process;
        }
    }

    /// <summary>
    /// Called when characters are reshuffled.
    /// Random reshuffle is called one per game - during game starting
    /// </summary>
    public class CharacterReshuffleEvent : Event
    {
        public readonly bool RandomReshuffle;
        public readonly Game.GameProcess GameProcess;

        public CharacterReshuffleEvent(bool RandomReshuffle, Game.GameProcess GameProcess) : base(false)
        {
            this.RandomReshuffle = RandomReshuffle;
            this.GameProcess = GameProcess;
        }

    }
    /// <summary>
    /// Called when game is adding plot card to the player
    /// </summary>
    public class PlotCardAddEvent : Event
    {
        private readonly PlotCardBase Card;
        private readonly Player PlayerAdded;
        private readonly GameProcess Process;

        public PlotCardAddEvent(PlotCardBase card, Player playerAdded, GameProcess process) : base(false)
        {
            Card = card;
            PlayerAdded = playerAdded;
            Process = process;
        }
    }
    /// <summary>
    /// Called when Money is added to player
    /// </summary>
    public class CashAddEvent : Event
    {
        public readonly int AddedCoins;
        public readonly Player Player;
        public readonly GameProcess GameProcess;
        public CashAddEvent(Player Player, int Coin, GameProcess process) : base(false)
        {
            this.GameProcess = process;
            this.AddedCoins = Coin;
            this.Player = Player;
        }
    }
    /// <summary>
    /// Called just before game start, before anything elsew
    /// </summary>
    public class PreGameStartEvent : Event
    {
        public readonly Game.GameProcess GameProcess;

        public PreGameStartEvent(GameProcess gameProcess) : base(false)
        {
            GameProcess = gameProcess;
        }
    }
    /// <summary>
    /// Called when someone is using PlotCard
    /// </summary>
    public class PlotCardUseEvent : Event
    {
        public readonly Player Executor, Receiver;
        public readonly GameProcess Process;
        public readonly PlotCardBase Card;

        public PlotCardUseEvent(Player executor, Player receiver, GameProcess process, PlotCardBase card) : base(false)
        {
            Executor = executor;
            Receiver = receiver;
            Process = process;
            Card = card;
        }
    }
    /// <summary>
    /// Called when players uses passive
    /// </summary>
    public class PassiveUseEvent : Event
    {
        public readonly Player Executor, Receiver;
        public readonly GameProcess Process;

        public PassiveUseEvent(Player executor, Player receiver, GameProcess process) : base(false)
        {
            Executor = executor;
            Receiver = receiver;
            Process = process;
        }
    }
    /// <summary>
    /// Called when player dies
    /// </summary>
    public class DeathEvent : Event
    {
        public readonly Player Killer, Killed;
        public readonly GameProcess Process;

        public DeathEvent(Player killer, Player killed, GameProcess process) : base(false)
        {
            Killer = killer;
            Killed = killed;
            Process = process;
        }
    }
    /// <summary>
    /// Called when player dies
    /// </summary>
    public class WinEvent : Event
    {
        public readonly Player Winner;
        public readonly GameProcess Process;

        public WinEvent(Player winner, GameProcess process) : base(false)
        {
            Winner = winner;
            Process = process;
        }
    }
}
