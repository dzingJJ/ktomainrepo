﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Reflection;
using KillTheOverlord.Events;

namespace KillTheOverlord.Game
{
    public class EventsManager
    {
        //To Copy-Paste
        public delegate void EventDelegate(Event even);
        public static event EventDelegate Event;
        internal static void CallEvent(Event evt)
        {
            throw new Exception();
        }

        public delegate void GameStartEventDelegate(PreGameStartEvent even);
        public static event GameStartEventDelegate PreGameStart;
        internal static void CallEvent(PreGameStartEvent evt)
        {
            if (PreGameStart != null)
                PreGameStart.Invoke(evt);
        }

        public delegate void CharacterReshuffleEventDelegate(CharacterReshuffleEvent even);
        public static event CharacterReshuffleEventDelegate CharacterReshuffle;
        internal static void CallEvent(CharacterReshuffleEvent evt)
        {
            if (CharacterReshuffle != null)
                CharacterReshuffle.Invoke(evt);
        }

        public delegate void CashAddEventDelegate(CashAddEvent even);
        public static event CashAddEventDelegate CashAdd;
        public static void CallEvent(CashAddEvent evt)
        {
            if (CashAdd != null)
                CashAdd.Invoke(evt);
        }

        public delegate void PlotCardAddEventDelegate(PlotCardAddEvent even);
        public static event PlotCardAddEventDelegate PlotCardAdd;
        internal static void CallEvent(PlotCardAddEvent evt)
        {
            if (PlotCardAdd != null)
                PlotCardAdd.Invoke(evt);
        }

        public delegate void PlotCardUseDelegate(PlotCardUseEvent even);
        public static event PlotCardUseDelegate PlotCardUse;
        internal static void CallEvent(PlotCardUseEvent evt)
        {
            if (PlotCardUse != null)
                PlotCardUse.Invoke(evt);
        }

        public delegate void ExecutionGiveDelegate(ExecutionGiveEvent even);
        public static event ExecutionGiveDelegate ExecutionGive;
        internal static void CallEvent(ExecutionGiveEvent evt)
        {
            if (ExecutionGive != null)
                ExecutionGive.Invoke(evt);
        }
        public delegate void PassiveUseDelegate(PassiveUseEvent even);
        public static event PassiveUseDelegate PassiveUse;
        internal static void CallEvent(PassiveUseEvent evt)
        {
            if (PassiveUse != null)
                PassiveUse.Invoke(evt);
        }
        public delegate void DeathDelegate(DeathEvent even);
        public static event DeathDelegate Death;
        internal static void CallEvent(DeathEvent evt)
        {
            if (Death != null)
                Death.Invoke(evt);
        }
    }
}
