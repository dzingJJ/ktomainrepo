﻿namespace GameWindow
{
    partial class ActualGame
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.Player1 = new System.Windows.Forms.Label();
            this.Player2 = new System.Windows.Forms.Label();
            this.Character2 = new System.Windows.Forms.Label();
            this.Character1 = new System.Windows.Forms.Label();
            this.Cash2 = new System.Windows.Forms.Label();
            this.Rank2 = new System.Windows.Forms.Label();
            this.Cash1 = new System.Windows.Forms.Label();
            this.Rank1 = new System.Windows.Forms.Label();
            this.cardsleft2 = new System.Windows.Forms.Label();
            this.cardsleft1 = new System.Windows.Forms.Label();
            this.Income1 = new System.Windows.Forms.Label();
            this.Income2 = new System.Windows.Forms.Label();
            this.hand2 = new System.Windows.Forms.Label();
            this.hand1 = new System.Windows.Forms.Label();
            this.tableLayoutPanel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 8;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 12.5F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 12.5F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 12.5F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 12.5F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 12.5F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 12.5F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 12.5F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 12.5F));
            this.tableLayoutPanel1.Controls.Add(this.Player1, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.Player2, 7, 0);
            this.tableLayoutPanel1.Controls.Add(this.Character2, 7, 1);
            this.tableLayoutPanel1.Controls.Add(this.Character1, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.Rank2, 7, 2);
            this.tableLayoutPanel1.Controls.Add(this.Rank1, 0, 2);
            this.tableLayoutPanel1.Controls.Add(this.cardsleft2, 6, 0);
            this.tableLayoutPanel1.Controls.Add(this.cardsleft1, 1, 0);
            this.tableLayoutPanel1.Controls.Add(this.Cash2, 6, 1);
            this.tableLayoutPanel1.Controls.Add(this.Cash1, 1, 1);
            this.tableLayoutPanel1.Controls.Add(this.Income1, 0, 3);
            this.tableLayoutPanel1.Controls.Add(this.Income2, 7, 3);
            this.tableLayoutPanel1.Controls.Add(this.hand2, 6, 2);
            this.tableLayoutPanel1.Controls.Add(this.hand1, 1, 2);
            this.tableLayoutPanel1.Location = new System.Drawing.Point(12, 12);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 4;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 35F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 15F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(914, 238);
            this.tableLayoutPanel1.TabIndex = 0;
            // 
            // Player1
            // 
            this.Player1.AutoSize = true;
            this.Player1.Location = new System.Drawing.Point(3, 0);
            this.Player1.Name = "Player1";
            this.Player1.Size = new System.Drawing.Size(91, 13);
            this.Player1.TabIndex = 0;
            this.Player1.Text = "Initialize Player1...";
            // 
            // Player2
            // 
            this.Player2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.Player2.AutoSize = true;
            this.Player2.Location = new System.Drawing.Point(820, 0);
            this.Player2.Name = "Player2";
            this.Player2.Size = new System.Drawing.Size(91, 13);
            this.Player2.TabIndex = 1;
            this.Player2.Text = "Initialize Player2...";
            // 
            // Character2
            // 
            this.Character2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.Character2.AutoSize = true;
            this.Character2.Location = new System.Drawing.Point(803, 59);
            this.Character2.Name = "Character2";
            this.Character2.Size = new System.Drawing.Size(108, 13);
            this.Character2.TabIndex = 2;
            this.Character2.Text = "Initialize Character2...";
            // 
            // Character1
            // 
            this.Character1.AutoSize = true;
            this.Character1.Location = new System.Drawing.Point(3, 59);
            this.Character1.Name = "Character1";
            this.Character1.Size = new System.Drawing.Size(108, 13);
            this.Character1.TabIndex = 3;
            this.Character1.Text = "Initialize Character1...";
            // 
            // Cash2
            // 
            this.Cash2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.Cash2.AutoSize = true;
            this.Cash2.Location = new System.Drawing.Point(709, 59);
            this.Cash2.Name = "Cash2";
            this.Cash2.Size = new System.Drawing.Size(86, 13);
            this.Cash2.TabIndex = 9;
            this.Cash2.Text = "Initialize Cash2...";
            // 
            // Rank2
            // 
            this.Rank2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.Rank2.AutoSize = true;
            this.Rank2.Location = new System.Drawing.Point(823, 118);
            this.Rank2.Name = "Rank2";
            this.Rank2.Size = new System.Drawing.Size(88, 13);
            this.Rank2.TabIndex = 11;
            this.Rank2.Text = "Initialize Rank2...";
            // 
            // Cash1
            // 
            this.Cash1.AutoSize = true;
            this.Cash1.Location = new System.Drawing.Point(117, 59);
            this.Cash1.Name = "Cash1";
            this.Cash1.Size = new System.Drawing.Size(86, 13);
            this.Cash1.TabIndex = 5;
            this.Cash1.Text = "Initialize Cash1...";
            // 
            // Rank1
            // 
            this.Rank1.AutoSize = true;
            this.Rank1.Location = new System.Drawing.Point(3, 118);
            this.Rank1.Name = "Rank1";
            this.Rank1.Size = new System.Drawing.Size(88, 13);
            this.Rank1.TabIndex = 7;
            this.Rank1.Text = "Initialize Rank1...";
            // 
            // cardsleft2
            // 
            this.cardsleft2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.cardsleft2.AutoSize = true;
            this.cardsleft2.Location = new System.Drawing.Point(702, 0);
            this.cardsleft2.Name = "cardsleft2";
            this.cardsleft2.Size = new System.Drawing.Size(93, 13);
            this.cardsleft2.TabIndex = 12;
            this.cardsleft2.Text = "Cards left: initialize";
            // 
            // cardsleft1
            // 
            this.cardsleft1.AutoSize = true;
            this.cardsleft1.Location = new System.Drawing.Point(117, 0);
            this.cardsleft1.Name = "cardsleft1";
            this.cardsleft1.Size = new System.Drawing.Size(93, 13);
            this.cardsleft1.TabIndex = 13;
            this.cardsleft1.Text = "Cards left: initialize";
            // 
            // Income1
            // 
            this.Income1.AutoSize = true;
            this.Income1.Location = new System.Drawing.Point(3, 177);
            this.Income1.Name = "Income1";
            this.Income1.Size = new System.Drawing.Size(96, 13);
            this.Income1.TabIndex = 14;
            this.Income1.Text = "Income1 initialize...";
            // 
            // Income2
            // 
            this.Income2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.Income2.AutoSize = true;
            this.Income2.Location = new System.Drawing.Point(815, 177);
            this.Income2.Name = "Income2";
            this.Income2.Size = new System.Drawing.Size(96, 13);
            this.Income2.TabIndex = 15;
            this.Income2.Text = "Income2 initialize...";
            // 
            // hand2
            // 
            this.hand2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.hand2.AutoSize = true;
            this.hand2.Location = new System.Drawing.Point(717, 118);
            this.hand2.Name = "hand2";
            this.hand2.Size = new System.Drawing.Size(78, 26);
            this.hand2.TabIndex = 16;
            this.hand2.Text = "Cards in hand: initialize...";
            // 
            // hand1
            // 
            this.hand1.AutoSize = true;
            this.hand1.Location = new System.Drawing.Point(117, 118);
            this.hand1.Name = "hand1";
            this.hand1.Size = new System.Drawing.Size(78, 26);
            this.hand1.TabIndex = 17;
            this.hand1.Text = "Cards in hand: initialize...";
            // 
            // ActualGame
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(938, 262);
            this.Controls.Add(this.tableLayoutPanel1);
            this.Name = "ActualGame";
            this.Text = "ActualGame";
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.Label Player1;
        private System.Windows.Forms.Label Player2;
        private System.Windows.Forms.Label Character2;
        private System.Windows.Forms.Label Character1;
        private System.Windows.Forms.Label Cash1;
        private System.Windows.Forms.Label Cash2;
        private System.Windows.Forms.Label Rank1;
        private System.Windows.Forms.Label Rank2;
        private System.Windows.Forms.Label cardsleft2;
        private System.Windows.Forms.Label cardsleft1;
        private System.Windows.Forms.Label Income1;
        private System.Windows.Forms.Label Income2;
        private System.Windows.Forms.Label hand2;
        private System.Windows.Forms.Label hand1;
    }
}