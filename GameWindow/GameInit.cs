﻿using KillTheOverloadFiles.Game;
using System;
using System.Windows.Forms;

namespace GameWindow
{
    public partial class GameInit : Form
    {
        int pNumber;

        public GameInit()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            // set players (max 2)
            pNumber = int.Parse(pNum.Text);
            if (pNumber > 2)
            {
                Console.Write("Too many players (max 2)");
                Environment.Exit(0);
            }
            ActualGame game = new ActualGame(pNumber);
            Hide();
            game.Show();
        }
    }
}
