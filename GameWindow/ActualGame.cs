﻿using KillTheOverloadFiles.Game;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace GameWindow
{
    public partial class ActualGame : Form
    {
        private void ActualGame_FormClosing1(object sender, FormClosingEventArgs e)
        {
            Environment.Exit(0);
        }

        public GameProcess process;

        public void SetPlayers(int p, GameProcess proc)
        {
            string hand = "";
            Player1.Text = proc.Game.Players[0].PlayerDetails.ToString();
            Character1.Text = proc.Game.Players[0].CurrentCharacter.UniqueName;
            Rank1.Text = "Rank: " + proc.Game.Players[0].CurrentCharacter.Rank.ToString();
            if (proc.Game.Players[0].CurrentCharacter.GoldPerTurn > 0)
                Income1.Text = "Gold per turn: +" + proc.Game.Players[0].CurrentCharacter.GoldPerTurn.ToString();
            else
                Income1.Text = "Gold per turn: " + proc.Game.Players[0].CurrentCharacter.GoldPerTurn.ToString();
            Cash1.Text = "Cash: " + proc.Game.Players[0].Coins.ToString();
            cardsleft1.Text = "Cards left: " + proc.Game.Players[0].PlotCardCount.ToString();
            for (int i = 0; i < proc.Game.Players[0].PlotCardCount; i++) {
                hand += proc.Game.Players[0].PlotCards[i].UniqueName + "\n";
                    }
            hand1.Text = "Cards in hand: \n" + hand;
            hand = "";
            if (p > 1)
            {
                Player2.Text = proc.Game.Players[1].PlayerDetails.ToString();
                Character2.Text = proc.Game.Players[1].CurrentCharacter.UniqueName;
                Rank2.Text = "Rank: " + proc.Game.Players[1].CurrentCharacter.Rank.ToString();
                if (proc.Game.Players[1].CurrentCharacter.GoldPerTurn > 0)
                    Income2.Text = "Gold per turn: +" + proc.Game.Players[1].CurrentCharacter.GoldPerTurn.ToString();
                else
                    Income2.Text = "Gold per turn: " + proc.Game.Players[1].CurrentCharacter.GoldPerTurn.ToString();
                Cash2.Text = "Cash: " + proc.Game.Players[1].Coins.ToString();
                cardsleft2.Text = "Cards left: " + proc.Game.Players[1].PlotCardCount.ToString();
                for (int i = 0; i < proc.Game.Players[1].PlotCardCount; i++)
                {
                    hand += proc.Game.Players[1].PlotCards[i].UniqueName + "\n";
                }
                hand2.Text = "Cards in hand: \n" + hand;
                hand = "";
            }
        }

        public ActualGame(int pNumber)
        {
            InitializeComponent();
            FormClosing += ActualGame_FormClosing1;
            PlayerDetails[] PlayerTable = new PlayerDetails[pNumber];
            for (int i = 0; i < pNumber; i++)
            {
                PlayerTable[i] = new PlayerDetails("Player" + i, i);
            }
            process = new GameProcess(PlayerTable);
            process.StartGame();
            SetPlayers(pNumber, process);
            Player Overlord = process.Game.GetOverlord();
            Player NotOverlord = null;
            for (int i = 0; i < pNumber; i++)
            {
                if (process.Game.Players[i] != Overlord)
                    NotOverlord = process.Game.Players[i];
            }
            process.UsePassive(Overlord, NotOverlord, out ErrorCodes error);
        }
    }
}
