﻿using KillTheOverloadFiles.Game;
using System;
using System.Windows.Forms;

namespace GameWindow
{
    public partial class Form1 : Form
    {
        GameProcess process;
        Player[] p;

        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            // set players (max 2)
            int pNumber = int.Parse(pNum.Text);
            if (pNumber > 2)
            {
                Console.Write("Too many players (max 2)");
                Environment.Exit(0);
            }
            result.Text = "";
            PlayerDetails[] Players = new PlayerDetails[pNumber];
            for (int i = 0; i < pNumber; i++)
            {
                Players[i] = new PlayerDetails("Player" + i, i);
                result.Text += Players[i].Nickname + " / " + Players[i].UniqueNumber + "\n";
            }
            process = new GameProcess(Players);
            process.StartGame();
        }
    }
}
